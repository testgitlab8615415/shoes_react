import React from 'react'
import Item from './Item'


const ListProduct = (props) => {
    const {data,HandleProductDetail}=props ;
  return (
 <div className="container">
     <div className="row">
     {    data.map((item) =>{
            return <Item HandleProductDetail={HandleProductDetail} item={item} key={item.id}/>
            //import component Item và sử dụng props để lấy dữ liệu từ bên ListProduct sang bên phía Item 
        })
     }</div>
     </div>
  
  )
}

export default ListProduct
